from django.shortcuts import render

def home (request):
    return render(request, 'seguimientoApp/index.html')

def rastreo (request):
    return render(request, 'seguimientoApp/rastreo.html')

def panel (request):
    return render(request, 'seguimientoApp/panel.html')
